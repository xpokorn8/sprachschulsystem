package org.fuseri.modulelanguageschool.course;

/**
 * An enum representing language proficiency levels based on the CEFR standard.
 */
public enum ProficiencyLevel {

    A1("Beginner"),
    A2("Elementary"),
    B1("Intermediate"),
    B2("Upper Intermediate"),
    C1("Advanced"),
    C2("Proficient"),
    C1N("Advanced Native speaker"),
    C2N("Proficient Native speaker");

    private final String description;

    /**
     * Constructor for LanguageLevel enum.
     *
     * @param description a String representing a brief description of the language proficiency level
     */
    ProficiencyLevel(String description) {
        this.description = description;
    }

    /**
     * Returns a brief description of the language proficiency level.
     *
     * @return a String representing the description of the language proficiency level
     */
    public String getDescription() {
        return description;
    }
}
