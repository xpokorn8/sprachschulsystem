package org.fuseri.modulelanguageschool.user;

import org.fuseri.modulelanguageschool.course.Course;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByEmail(String email);

    Boolean existsByEmail(String email);

    @Query("SELECT c FROM Course c Left Join FETCH User d WHERE d.id = :id AND c.finished=FALSE")
    List<Course> getEnrolled(Long id);

    @Query("SELECT c FROM Course c Left Join FETCH User d WHERE d.id = :id AND c.finished=TRUE")
    List<Course> getFinished(Long id);

}
