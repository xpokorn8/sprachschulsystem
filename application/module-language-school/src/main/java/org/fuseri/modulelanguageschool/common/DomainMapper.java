package org.fuseri.modulelanguageschool.common;
import org.fuseri.model.dto.common.DomainObjectDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

public interface DomainMapper<T extends DomainObject, S extends DomainObjectDto> {

    T fromDto(S dto);

    S toDto(T entity);

    List<S> toDtoList(List<T> entities);

    default Page<S> toDtoPage(Page<T> entities) {
        return new PageImpl<>(toDtoList(entities.getContent()), entities.getPageable(), entities.getTotalPages());
    }
}
