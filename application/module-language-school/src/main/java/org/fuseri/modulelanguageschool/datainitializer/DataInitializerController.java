package org.fuseri.modulelanguageschool.datainitializer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.fuseri.modulelanguageschool.ModuleLanguageSchoolApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/datainitializer")
public class DataInitializerController {
    private final DataInitializer dataInitializer;

    @Autowired
    public DataInitializerController(DataInitializer dataInitializer) {
        this.dataInitializer = dataInitializer;
    }

    @Operation(security = @SecurityRequirement(name = ModuleLanguageSchoolApplication.SECURITY_SCHEME_NAME,scopes = {"test_1"})
            ,summary = "Seed language school database", description = "Seeds language school database. Drops all data first")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Data initialized successfully."),
    })
    @PostMapping
    public ResponseEntity<Void> initialize(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal principal) {
        dataInitializer.initialize();
        return ResponseEntity.noContent().build();
    }

    @Operation(security = @SecurityRequirement(name = ModuleLanguageSchoolApplication.SECURITY_SCHEME_NAME,scopes = {"test_1"})
            ,summary = "Drop language school database", description = "Drops all data from language school database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Data dropped successfully."),
    })
    @DeleteMapping
    public ResponseEntity<Void> drop(@AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal principal) {
        dataInitializer.drop();
        return ResponseEntity.noContent().build();
    }

}
