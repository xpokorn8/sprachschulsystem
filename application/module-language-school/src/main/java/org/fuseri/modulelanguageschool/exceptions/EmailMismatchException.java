package org.fuseri.modulelanguageschool.exceptions;

public class EmailMismatchException extends RuntimeException {
    public EmailMismatchException() {
    }

    public EmailMismatchException(String message) {
        super(message);
    }
}
