package org.fuseri.modulelanguageschool.course;

/**
 * An enum representing individual languages.
 */
public enum Language {

    ENGLISH("English"),
    GERMAN("Deutsch"),
    SPANISH("Español"),
    CZECH("Čeština");

    private final String nativeName;

    /**
     * Constructor for Language enum.
     *
     * @param nativeName a String representing the native name of the language
     */
    Language(String nativeName) {
        this.nativeName = nativeName;
    }

    /**
     * Returns the native name of the language.
     *
     * @return a String representing the native name of the language
     */
    public String getNativeName() {
        return nativeName;
    }
}
