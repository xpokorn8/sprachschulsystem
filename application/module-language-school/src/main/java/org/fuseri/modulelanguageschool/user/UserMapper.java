package org.fuseri.modulelanguageschool.user;

import org.fuseri.model.dto.user.UserCreateDto;
import org.fuseri.model.dto.user.UserDto;
import org.fuseri.modulelanguageschool.common.DomainMapper;
import org.mapstruct.Mapper;

@Mapper
public interface UserMapper extends DomainMapper<User, UserDto> {
    User fromCreateDto(UserCreateDto dto);
}
