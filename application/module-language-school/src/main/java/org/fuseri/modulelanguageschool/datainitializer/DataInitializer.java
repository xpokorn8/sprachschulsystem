package org.fuseri.modulelanguageschool.datainitializer;


import org.fuseri.modulelanguageschool.course.Course;
import org.fuseri.modulelanguageschool.course.CourseRepository;
import org.fuseri.modulelanguageschool.course.Language;
import org.fuseri.modulelanguageschool.course.ProficiencyLevel;
import org.fuseri.modulelanguageschool.lecture.Lecture;
import org.fuseri.modulelanguageschool.lecture.LectureRepository;
import org.fuseri.modulelanguageschool.user.User;
import org.fuseri.modulelanguageschool.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class DataInitializer {

    private final CourseRepository courseRepository;
    private final LectureRepository lectureRepository;
    private final UserRepository userRepository;

    Course c1;
    Course c2;
    Course c3;
    Course c4;
    Course c5;
    Course c6;
    private Lecture germanLectureIntro;
    private User germanLecturer;
    private User englishLecturer;
    private Lecture englishLectureIntro;
    private Lecture englishLectureTense;


    @Autowired
    public DataInitializer(CourseRepository courseRepository, LectureRepository lectureRepository,
                           UserRepository userRepository) {
        this.courseRepository = courseRepository;
        this.lectureRepository = lectureRepository;
        this.userRepository = userRepository;
    }

    public void drop() {
        lectureRepository.deleteAll();
        courseRepository.deleteAll();
        userRepository.deleteAll();
    }

    public void initialize() {
        drop();
        initializeCourseAndLecture();
        initializeUser();
    }

    private void initializeCourseAndLecture() {
        c1 = new Course("German for Beginners", 25, Language.GERMAN, ProficiencyLevel.A1, new ArrayList<>());
        c2 = new Course("English A1", 25, Language.ENGLISH, ProficiencyLevel.A1, new ArrayList<>());
        c3 = new Course("English A2", 25, Language.ENGLISH, ProficiencyLevel.A2, new ArrayList<>());
        c4 = new Course("English B1", 25, Language.ENGLISH, ProficiencyLevel.B1, new ArrayList<>());
        c5 = new Course("English B2", 25, Language.ENGLISH, ProficiencyLevel.B2, new ArrayList<>());
        c6 = new Course("Spanish B1", 25, Language.SPANISH, ProficiencyLevel.B1, new ArrayList<>());

        courseRepository.saveAll(List.of(c1, c2, c3, c4, c5, c6));

        germanLectureIntro = new Lecture(LocalDateTime.of(2023, 5, 2, 10, 0), LocalDateTime.of(2023, 5, 2, 12, 0),
                "Introduction to course", 25, c1, germanLecturer);
        var l2 = new Lecture(LocalDateTime.of(2023, 5, 4, 10, 0), LocalDateTime.of(2023, 5, 4, 12, 0),
                "Begrüßung und Vorstellung", 25, c1, germanLecturer);
        var l3 = new Lecture(LocalDateTime.of(2023, 5, 9, 10, 0), LocalDateTime.of(2023, 5, 9, 12, 0),
                "Mein Wohnort und meine Wohnung", 25, c1, germanLecturer);
        var l4 = new Lecture(LocalDateTime.of(2023, 5, 11, 10, 0), LocalDateTime.of(2023, 5, 11, 12, 0),
                "Essen und Trinken", 25, c1, germanLecturer);
        var l5 = new Lecture(LocalDateTime.of(2023, 5, 16, 10, 0), LocalDateTime.of(2023, 5, 16, 12, 0),
                "Meine Familie und Freunde", 25, c1, germanLecturer);

        englishLectureIntro = new Lecture(LocalDateTime.of(2023, 5, 1, 10, 0), LocalDateTime.of(2023, 5, 1, 12, 0),
                "Introduction to course", 25, c5, englishLecturer);
        englishLectureTense = new Lecture(LocalDateTime.of(2023, 5, 6, 10, 0), LocalDateTime.of(2023, 5, 6, 12, 0),
                "Repetition of english tenses", 25, c5, englishLecturer);
        var l8 = new Lecture(LocalDateTime.of(2023, 5, 8, 10, 0), LocalDateTime.of(2023, 5, 8, 12, 0),
                "Food", 25, c5, englishLecturer);
        var l9 = new Lecture(LocalDateTime.of(2023, 5, 8, 10, 0), LocalDateTime.of(2023, 5, 8, 12, 0),
                "School systems around the world", 25, c5, englishLecturer);
        var l10 = new Lecture(LocalDateTime.of(2023, 5, 15, 10, 0), LocalDateTime.of(2023, 5, 15, 12, 0),
                "Interview questions", 25, c5, englishLecturer);

        lectureRepository.saveAll(List.of(germanLectureIntro, l2, l3, l4, l5, englishLectureIntro, englishLectureTense, l8, l9, l10));
    }

    private void initializeUser() {
        // admin
        var a1 = new User("johndoe", "john.doe@example.com", "John", "Doe");
        var a2 = new User("janesmith", "jane.smith@example.com", "Jane", "Smith");

        // lecturer
        englishLecturer = new User("anaken", "ana.kentinsky@example.com", "Ana", "Kentinsky");
        var l4 = new User("michaeljones", "michael.jones@example.com", "Michael", "Jones");
        var l5 = new User("clairedupont",  "claire.dupont@example.com", "Claire", "Dupont");
        germanLecturer = new User("maxmustermann", "max.mustermann@example.com", "Max", "Mustermann");

        // students
        User s7 = new User("samuelsmith", "samuel.smith@example.com", "Samuel", "Smith");

        User s8 = new User("jessicanguyen", "jessica.nguyen@example.com", "Jessica", "Nguyen");

        User s9 = new User("peterhansen", "peter.hansen@example.com", "Peter", "Hansen");

        User s10 = new User("luciedupont", "lucie.dupont@example.com", "Lucie", "Dupont");

        User s11 = new User("hansschmidt", "hans.schmidt@example.com", "Hans", "Schmidt");

        User s12 = new User("emmajones", "emma.jones@example.com", "Emma", "Jones");

        User s13 = new User("oliversmith", "oliver.smith@example.com", "Oliver", "Smith");

        User s14 = new User("lauragarcia", "laura.garcia@example.com", "Laura", "Garcia");

        User s15 = new User("felixmueller", "felix.mueller@example.com", "Felix", "Mueller");

        User s16 = new User("emiliedupont", "emilie.dupont@example.com", "Emilie", "Dupont");

        User s17 = new User("peterkovac", "pkovac@example.com", "Peter", "Kovac");

        User s18 = new User("davidprachar", "prachar@example.com", "David", "Prachar");

        // language_proficiency
        englishLecturer.addLanguageProficiency(Language.ENGLISH, ProficiencyLevel.C2N);
        l4.addLanguageProficiency(Language.ENGLISH, ProficiencyLevel.C1N);
        l4.addLanguageProficiency(Language.SPANISH, ProficiencyLevel.C1);
        l5.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.C1);
        germanLecturer.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.C1N);

        s7.addLanguageProficiency(Language.ENGLISH, ProficiencyLevel.B1);
        s9.addLanguageProficiency(Language.ENGLISH, ProficiencyLevel.B1);
        s11.addLanguageProficiency(Language.ENGLISH, ProficiencyLevel.B1);

        s8.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.B2);

        s10.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.A1);
        s11.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.A1);
        s12.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.A1);
        s13.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.A1);
        s14.addLanguageProficiency(Language.GERMAN, ProficiencyLevel.A1);

        s15.addLanguageProficiency(Language.CZECH, ProficiencyLevel.A1);

        s16.addLanguageProficiency(Language.SPANISH, ProficiencyLevel.B1);
        s17.addLanguageProficiency(Language.SPANISH, ProficiencyLevel.A2);
        s18.addLanguageProficiency(Language.SPANISH, ProficiencyLevel.B1);

        // course - user
        courseEnrolStudents(c1, List.of(s10, s11, s12, s13, s14));
        c2.enrolStudent(s7);
        courseEnrolStudents(c3, List.of(s7, s9));
        courseEnrolStudents(c4, List.of(s7, s9));
        courseEnrolStudents(c5, List.of(s7, s9, s11));

        // lecture - user
        lectureEnrolStudents(germanLectureIntro, List.of(s10, s11, s12, s13, s14));
        lectureEnrolStudents(englishLectureIntro, List.of(s7, s9));
        lectureEnrolStudents(englishLectureIntro, List.of(s7, s9, s11));

        userRepository.saveAll(List.of(a1, a2, englishLecturer, l4, l5, germanLecturer,
                s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, s17, s18));

        courseRepository.save(c1);
        lectureRepository.saveAll(List.of(germanLectureIntro, englishLectureIntro, englishLectureTense));
    }

    private void courseEnrolStudents(Course c, List<User> students) {
        for (var s : students) {
            c.enrolStudent(s);
        }
    }

    private void lectureEnrolStudents(Lecture l, List<User> students) {
        for (var s : students) {
            l.enrolStudent(s);
        }
    }
}
