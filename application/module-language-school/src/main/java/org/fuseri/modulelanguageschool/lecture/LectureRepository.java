package org.fuseri.modulelanguageschool.lecture;
import org.fuseri.modulelanguageschool.course.Language;
import org.fuseri.modulelanguageschool.course.ProficiencyLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LectureRepository  extends JpaRepository<Lecture, Long> {

    @Query("SELECT l FROM Lecture l WHERE l.course.id = ?1")
    List<Lecture> findAllByCourse(Long id);

    @Query("SELECT l FROM Lecture l WHERE l.course.language = ?1")
    List<Lecture> findAllByLang(Language language);

    @Query("SELECT l FROM Lecture l WHERE l.course.language = ?1 AND l.course.proficiency = ?2")
    List<Lecture> findAllByLangProf(Language language, ProficiencyLevel proficiencyLevel);
}
