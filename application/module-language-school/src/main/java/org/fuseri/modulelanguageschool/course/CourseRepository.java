package org.fuseri.modulelanguageschool.course;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CourseRepository extends JpaRepository<Course, Long> {


    @Query("SELECT c FROM Course c left join fetch User u WHERE c.language = ?1")
    Course getById(Long id);

    @Query("SELECT c FROM Course c WHERE c.language = ?1")
    List<Course> findAllByLang(Language language);

    @Query("SELECT c FROM Course c WHERE c.language = ?1 AND c.proficiency = ?2")
    List<Course> findAllByLangProf(Language language, ProficiencyLevel proficiencyLevel);
}
