package org.fuseri.modulelanguageschool.common;

public class UserWithEmailAlreadyExists extends RuntimeException {
    public UserWithEmailAlreadyExists() {
    }

    public UserWithEmailAlreadyExists(String message) {
        super(message);
    }

    public UserWithEmailAlreadyExists(String message, Throwable cause) {
        super(message, cause);
    }

    public UserWithEmailAlreadyExists(Throwable cause) {
        super(cause);
    }

    public UserWithEmailAlreadyExists(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
