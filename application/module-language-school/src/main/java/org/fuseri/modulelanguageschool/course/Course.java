package org.fuseri.modulelanguageschool.course;

import jakarta.persistence.*;
import lombok.*;
import org.fuseri.modulelanguageschool.common.DomainObject;
import org.fuseri.modulelanguageschool.user.User;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "course")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Course extends DomainObject {

    private String name;
    private Integer capacity;

    @Enumerated(EnumType.STRING)
    private Language language;

    @Enumerated(EnumType.STRING)
    private ProficiencyLevel proficiency;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> students;

    private boolean finished = false;

    public Course(String name, int capacity, Language language, ProficiencyLevel proficiency) {
        this.name = name;
        this.capacity = capacity;
        this.language = language;
        this.proficiency = proficiency;
        this.students = new ArrayList<>();
    }

    public void enrolStudent(User student) {
        students.add(student);
    }

    public void expelStudent(User student) {
        students.remove(student);
    }

    public Course(String name, Integer capacity, Language language, ProficiencyLevel proficiency) {
        this.name = name;
        this.capacity = capacity;
        this.language = language;
        this.proficiency = proficiency;
    }

    public Course(String name, Integer capacity, Language language, ProficiencyLevel proficiency, List<User> students) {
        this.name = name;
        this.capacity = capacity;
        this.language = language;
        this.proficiency = proficiency;
        this.students = students;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }
}
