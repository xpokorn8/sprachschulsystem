package org.fuseri.modulelanguageschool.lecture;

import jakarta.persistence.*;
import lombok.*;
import org.fuseri.modulelanguageschool.common.DomainObject;
import org.fuseri.modulelanguageschool.course.Course;
import org.fuseri.modulelanguageschool.user.User;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a lecture entity in the domain model.
 */
@Getter
@Setter
@Entity
@Table(name = "lecture")
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class Lecture extends DomainObject {

    private LocalDateTime lectureFrom;
    private LocalDateTime lectureTo;
    private String topic;
    private Integer capacity;

    @ManyToOne(fetch = FetchType.LAZY)
    private Course course;

    @ManyToOne(fetch = FetchType.LAZY)
    private User lecturer;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<User> students;

    public Lecture(LocalDateTime lectureFrom, LocalDateTime lectureTo,
                   String topic, Integer capacity, Course course, User lecturer) {
        this.lectureFrom = lectureFrom;
        this.lectureTo = lectureTo;
        this.topic = topic;
        this.capacity = capacity;
        this.course = course;
        this.lecturer = lecturer;
        this.students = new ArrayList<>();
    }

    public void enrolStudent(User student) {
        students.add(student);
    }

    public void expelStudent(User student) {
        students.remove(student);
    }
}
