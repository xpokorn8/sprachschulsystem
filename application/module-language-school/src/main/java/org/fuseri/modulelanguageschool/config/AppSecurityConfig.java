package org.fuseri.modulelanguageschool.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@EnableWebMvc
public class AppSecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();
        httpSecurity.authorizeHttpRequests(x -> x
                // "/users/register" is for automatic registering upon authentication
                .requestMatchers(
                        "/actuator/**",
                        "/swagger-ui/**",
                        "/v3/api-docs/**",
                        "/datainitializer").permitAll()

                // PUT
                .requestMatchers(HttpMethod.PUT, "/courses/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                // POST
                .requestMatchers(HttpMethod.POST, "/courses/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                // PATCH
                .requestMatchers(HttpMethod.PATCH, "/courses/enrolStudent/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                .requestMatchers(HttpMethod.PATCH, "/courses/expelStudent/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                // DELETE
                .requestMatchers(HttpMethod.DELETE, "/courses/**").hasAuthority("SCOPE_test_1")


                // PUT
                .requestMatchers(HttpMethod.PUT, "/lectures/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                // POST
                .requestMatchers(HttpMethod.POST, "/lectures/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                // PATCH
                .requestMatchers(HttpMethod.PATCH, "/lectures/setLecturer/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                .requestMatchers(HttpMethod.PATCH, "/lectures/enrolStudent/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                .requestMatchers(HttpMethod.PATCH, "/lectures/expelStudent/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")
                // DELETE
                .requestMatchers(HttpMethod.DELETE, "/lectures/**").hasAnyAuthority("SCOPE_test_1", "SCOPE_test_2")


                // PUT
                .requestMatchers(HttpMethod.PUT, "/users/**").hasAuthority("SCOPE_test_1")
                // POST
                .requestMatchers(HttpMethod.POST, "/users").hasAuthority("SCOPE_test_1")
                .requestMatchers(HttpMethod.POST, "/users/").hasAuthority("SCOPE_test_1")
                // DELETE
                .requestMatchers(HttpMethod.DELETE, "/users/**").hasAuthority("SCOPE_test_1")
                .anyRequest().authenticated()
        ).oauth2ResourceServer(OAuth2ResourceServerConfigurer::opaqueToken)
        ;
        return httpSecurity.build();
    }
}
