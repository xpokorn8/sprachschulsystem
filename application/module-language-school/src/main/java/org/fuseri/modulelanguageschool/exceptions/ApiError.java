package org.fuseri.modulelanguageschool.exceptions;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.ToString;
import org.springframework.http.HttpStatus;

import java.time.Clock;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@ToString
class ApiError {

    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private List<ApiSubError> subErrors;
    private String path;

    private ApiError() {
        timestamp = LocalDateTime.now(Clock.systemUTC());
    }

    ApiError(HttpStatus status, Throwable ex, String path) {
        this();
        this.status = status;
        this.path = path;
        this.message = ex.getLocalizedMessage();
    }

    ApiError(HttpStatus status, List<ApiSubError> subErrors, Throwable ex, String path) {
        this();
        this.status = status;
        this.subErrors = subErrors;
        this.path = path;
        this.message = ex.getLocalizedMessage();
    }
}
