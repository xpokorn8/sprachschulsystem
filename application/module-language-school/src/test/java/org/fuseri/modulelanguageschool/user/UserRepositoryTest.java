package org.fuseri.modulelanguageschool.user;

import org.fuseri.modulelanguageschool.course.Course;
import org.fuseri.modulelanguageschool.course.Language;
import org.fuseri.modulelanguageschool.course.ProficiencyLevel;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TestEntityManager entityManager;

    private final Course course = new Course("AJ1", 10, Language.ENGLISH, ProficiencyLevel.B2);
    private final Set<Course> COURSES = Set.of(course);
    private final User user = new User(
            "xnovak", "xnovak@emample.com", "Peter", "Novak", COURSES, Map.of());

    @Test
    void getEnrolled() {
        entityManager.persist(course);
        entityManager.persist(user);
        entityManager.flush();

        List<Course> foundCourses = userRepository.getEnrolled(user.getId());
        assertNotNull(foundCourses);
        assertEquals(new ArrayList<>(COURSES), foundCourses);
    }

    @Test
    void getFinished() {
        course.setFinished(true);
        entityManager.persist(course);
        entityManager.persist(user);
        entityManager.flush();

        List<Course> foundCourses = userRepository.getFinished(user.getId());
        assertNotNull(foundCourses);
        assertEquals(new ArrayList<>(COURSES), foundCourses);
    }
}