package org.fuseri.model.dto.exercise;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class AnswersCreateDto {

    @NotNull(message = "answers must belong to a question, questionId can not be null")
    private long questionId;

    @Valid
    private List<AnswerInQuestionCreateDto> answers;
}
