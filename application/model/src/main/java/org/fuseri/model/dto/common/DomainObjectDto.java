package org.fuseri.model.dto.common;

import jakarta.validation.constraints.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode(exclude = "id")
public abstract class DomainObjectDto {

    @NotNull
    private Long id;
}
