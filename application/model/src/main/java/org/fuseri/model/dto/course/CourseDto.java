package org.fuseri.model.dto.course;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a Data Transfer Object (DTO) for Course entities.
 * It is used to transfer Course data between different layers of the application.
 * It extends the DomainObjectDto class and includes additional Course-specific fields.
 */
@Schema(example = """
        {
          "id": 1,
          "name": "english a1",
          "capacity": 10,
          "language": "ENGLISH",
          "proficiency": "A1",
          "students": [],
          "finished": false
        }
                """)
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class CourseDto extends DomainObjectDto {

    @NotBlank(message = "Course name is required")
    @Size(max = 63, message = "Course name must not exceed {max} characters")
    private String name;

    @NotNull(message = "Lecture capacity cannot be null")
    @Min(value = 1, message = "Lecture capacity must be at least 1")
    private Integer capacity;

    @NotNull(message = "Language type is required")
    @Valid
    private LanguageTypeDto language;

    @NotNull(message = "Proficiency level is required")
    @Valid
    private ProficiencyLevelDto proficiency;

    @NotNull(message = "Student's list is required")
    @Valid
    private List<Long> students;

    @NotNull(message = "Finished status required")
    @Valid
    private Boolean finished;

    public CourseDto(String name, Integer capacity, LanguageTypeDto languageTypeDto, ProficiencyLevelDto proficiencyLevelDto, List<Long> students, Boolean finished) {
        this.name = name;
        this.capacity = capacity;
        this.language = languageTypeDto;
        this.proficiency = proficiencyLevelDto;
        this.students = students;
        this.finished = finished;
    }

    public CourseDto(String name, int capacity, LanguageTypeDto languageTypeDto, ProficiencyLevelDto proficiencyLevelDto) {
        this.name = name;
        this.capacity = capacity;
        this.language = languageTypeDto;
        this.proficiency = proficiencyLevelDto;
        this.students = new ArrayList<>();
    }
}
