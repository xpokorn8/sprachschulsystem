package org.fuseri.model.dto.certificate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;


/**
 * This class represents a Data Transfer Object (DTO) for Certificate file.
 * It is used to transfer Certificate file between different layers of the application.
 * It extends the DomainObjectDto class and includes additional CertificateFile-specific fields.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CertificateFileDto extends DomainObjectDto {
    @NotBlank
    @NotNull
    private String filename;
    @NotBlank
    @NotNull
    private String fileContentBase64;
}

