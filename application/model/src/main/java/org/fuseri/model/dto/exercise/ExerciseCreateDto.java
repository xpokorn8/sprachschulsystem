package org.fuseri.model.dto.exercise;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ExerciseCreateDto {
    @NotBlank(message = "exercise name is required")
    private String name;

    @NotBlank(message = "exercise description is required")
    private String description;

    @NotNull(message = "exercise difficulty is required")
    @PositiveOrZero
    private int difficulty;

    @NotNull(message = "exercise must belong to a course, courseId can not be null")
    private long courseId;
}
