package org.fuseri.model.dto.course;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class represents a Data Transfer Object (DTO) for Course entities.
 * It is used to create a new Course with the init data.
 */
@Schema(example = """
        {
          "name": "english a1",
          "capacity": 10,
          "language": "ENGLISH",
          "proficiency": "A1"
        }
        """)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CourseCreateDto {

    @NotBlank(message = "Course name is required")
    @Size(max = 50, message = "Course name must not exceed {max} characters")
    private String name;

    @NotNull(message = "Lecture capacity cannot be null")
    @Min(value = 1, message = "Lecture capacity must be at least 1")
    private Integer capacity;

    @NotNull(message = "Language type is required")
    @Valid
    private LanguageTypeDto language;

    @NotNull(message = "Proficiency level is required")
    @Valid
    private ProficiencyLevelDto proficiency;

}
