package org.fuseri.model.dto.exercise;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class QuestionUpdateDto {

    @NotBlank(message = "question text is required")
    private String text;

    @NotNull(message = "question must belong to an exercise, exerciseId can not be null")
    private long exerciseId;
}
