package org.fuseri.model.dto.exercise;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.fuseri.model.dto.common.DomainObjectDto;

@AllArgsConstructor
@Getter
@EqualsAndHashCode(callSuper = false)
public class AnswerDto extends DomainObjectDto {

    @NotBlank
    private String text;

    @NotNull
    private boolean correct;
}
