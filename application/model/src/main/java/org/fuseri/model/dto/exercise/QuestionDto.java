package org.fuseri.model.dto.exercise;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class QuestionDto extends DomainObjectDto {

    @NotBlank
    private String text;

    @NotNull
    private long exerciseId;

    @Valid
    private List<AnswerDto> answers;
}
