package org.fuseri.model.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.*;
import org.fuseri.model.dto.common.DomainObjectDto;
import org.fuseri.model.dto.course.LanguageTypeDto;
import org.fuseri.model.dto.course.ProficiencyLevelDto;

import java.util.Map;

@Schema(example = """
        {
          "id": 1,
          "username": "adelkaxxx",
          "email": "adelkaxxx@muni.mail.cz",
          "firstName": "Adéla",
          "lastName": "Pulcová",
          "languageProficiency": {
            "CZECH": "A2"
          }
        }
        """)
@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
@ToString
@NoArgsConstructor
public class UserDto extends DomainObjectDto {

    @NotBlank
    private String username;

    @NotBlank
    private String email;

    @NotBlank
    private String firstName;

    @NotBlank
    private String lastName;

    @NotNull
    @Valid
    private Map<LanguageTypeDto, ProficiencyLevelDto> languageProficiency;



    public UserDto(String username, String email, String firstName, String lastName, Map<LanguageTypeDto, ProficiencyLevelDto> languageProficiency) {
        setId(0L);
        this.username = username;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.languageProficiency = languageProficiency;
    }
}
