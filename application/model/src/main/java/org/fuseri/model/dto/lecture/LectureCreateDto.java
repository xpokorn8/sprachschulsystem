package org.fuseri.model.dto.lecture;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Schema(example = """
        {
          "lectureFrom": "2069-04-20T22:24:33.038Z",
          "lectureTo": "2069-04-20T23:24:33.038Z",
          "topic": "pronouns",
          "capacity": 10,
          "courseId": 1
        }
        """)
@Getter
@Setter
@NoArgsConstructor
public class LectureCreateDto {

    @Future(message = "Lecture start date and time must be in the future")
    @NotNull(message = "Lecture start date and time cannot be null")
    private LocalDateTime lectureFrom;

    @Future(message = "Lecture end date and time must be in the future")
    @NotNull(message = "Lecture end date and time cannot be null")
    private LocalDateTime lectureTo;

    @NotBlank(message = "Lecture topic cannot be blank")
    @Size(max = 255, message = "Lecture topic must be no more than {max} characters")
    private String topic;

    @NotNull(message = "Lecture capacity cannot be null")
    @Min(value = 1, message = "Lecture capacity must be at least 1")
    private Integer capacity;

    @NotNull(message = "Lecture course cannot be null")
    private Long courseId;

    public LectureCreateDto(LocalDateTime from, LocalDateTime to, String topic, Integer capacity, Long courseId) {
        this.lectureFrom = from;
        this.lectureTo = to;
        this.topic = topic;
        this.capacity = capacity;
        this.courseId = courseId;
    }

    @AssertTrue(message = "Lecture start datetime must be before Lecture end datetime")
    private boolean isFromDateBeforeToDate() {
        if (lectureFrom == null || lectureTo == null) return true;
        return lectureFrom.isBefore(lectureTo);
    }
}
