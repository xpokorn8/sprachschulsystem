package org.fuseri.model.dto.exercise;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;

@Getter
@Setter
@EqualsAndHashCode(callSuper = false)
public class ExerciseDto extends DomainObjectDto {

    @NotBlank
    private String name;

    @NotBlank
    private String description;

    @NotNull
    @PositiveOrZero
    private int difficulty;

    @NotNull
    private long courseId;
}
