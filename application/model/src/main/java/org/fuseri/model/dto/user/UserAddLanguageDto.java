package org.fuseri.model.dto.user;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fuseri.model.dto.course.LanguageTypeDto;
import org.fuseri.model.dto.course.ProficiencyLevelDto;

@Schema(example = """
        {
          "language": "ENGLISH",
          "proficiency": "A1"
        }
        """)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserAddLanguageDto {

    @NotNull
    @Valid
    LanguageTypeDto language;

    @NotNull
    @Valid
    ProficiencyLevelDto proficiency;
}
