package org.fuseri.model.dto.exercise;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class AnswerInQuestionCreateDto {

    @NotBlank(message = "answer text is required")
    private String text;

    @NotNull(message = "answer correctness must be specified")
    private boolean correct;
}
