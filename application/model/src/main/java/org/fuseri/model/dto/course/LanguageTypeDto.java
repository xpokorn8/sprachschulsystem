package org.fuseri.model.dto.course;

public enum LanguageTypeDto {
    ENGLISH,
    GERMAN,
    SPANISH,
    CZECH
}
