package org.fuseri.model.dto.certificate;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fuseri.model.dto.course.CourseCertificateDto;
import org.fuseri.model.dto.user.UserDto;


/**
 * This class represents a Data Transfer Object (DTO) for creating Certificate entities.
 * It is used for creating Certificate entity.
 */
@Schema(example = """
        {
           "user": {
                   "id": 1,
                   "username": "adelkaxxx",
                   "email": "adelkaxxx@muni.mail.cz",
                   "firstName": "Adéla",
                   "lastName": "Pulcová",
                   "address": {
                     "country": "Czechia",
                     "city": "Praha",
                     "street": "Bubenské nábřeží",
                     "houseNumber": "306/13",
                     "zip": "170 00"
                   },
                   "userType": "STUDENT",
                   "languageProficiency": {
                     "CZECH": "A2"
                   }
                 },
           "course": {
                   "id": 1,
                   "name": "english a1",
                   "capacity": 10,
                   "language": "ENGLISH",
                   "proficiency": "A1"
                 }
         
         }
        """)
@Getter
@Setter
@NoArgsConstructor
public class CertificateCreateDto {
    @NotNull
    @Valid
    private UserDto user;
    @NotNull
    @Valid
    private CourseCertificateDto course;

    public CertificateCreateDto(UserDto user, CourseCertificateDto course) {
        this.user = user;
        this.course = course;
    }
}
