package org.fuseri.model.dto.exercise;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class QuestionCreateDto {

    @NotBlank(message = "question text is required")
    private String text;

    @NotNull(message = "question must belong to an exercise, exerciseId can not be null")
    private long exerciseId;

    @Valid
    private List<AnswerInQuestionCreateDto> answers;
}
