package org.fuseri.model.dto.certificate;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;
import org.fuseri.model.dto.course.CourseDto;
import org.fuseri.model.dto.user.UserDto;

import java.time.Instant;

/**
 * This class represents a Data Transfer Object (DTO) for Certificate entities.
 * It is used to transfer Certificate data between different layers of the application.
 * It extends the DomainObjectDto class and includes additional Certificate-specific fields.
 */
@Getter
@Setter
@AllArgsConstructor
public class CertificateDto extends DomainObjectDto {
    @NotBlank
    @NotNull
    private UserDto user;
    @NotBlank
    @NotNull
    private Instant generatedAt;
    @NotNull
    @Valid
    private CourseDto course;
    @NotNull
    @Valid
    private CertificateFileDto certificateFile;

    public CertificateDto() {
        setId(0L);
    }
}
