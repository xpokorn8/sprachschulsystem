package org.fuseri.model.dto.course;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;

/**
 * This class represents a Data Transfer Object (DTO) for Course entities.
 * It is used for passing Course data to Certificate module.
 * It extends the DomainObjectDto class and includes additional Course-specific fields.
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class CourseCertificateDto extends DomainObjectDto {

    @NotBlank(message = "Course name is required")
    @Size(max = 63, message = "Course name must not exceed {max} characters")
    private String name;

    @NotNull(message = "Lecture capacity cannot be null")
    @Min(value = 1, message = "Lecture capacity must be at least 1")
    private Integer capacity;

    @NotNull(message = "Language type is required")
    @Valid
    private LanguageTypeDto language;

    @NotNull(message = "Proficiency level is required")
    @Valid
    private ProficiencyLevelDto proficiency;

    public CourseCertificateDto(String name, Integer capacity, LanguageTypeDto languageTypeDto, ProficiencyLevelDto proficiencyLevelDto) {
        setId(0L);
        this.name = name;
        this.capacity = capacity;
        this.language = languageTypeDto;
        this.proficiency = proficiencyLevelDto;
    }

}
