package org.fuseri.model.dto.certificate;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;
import org.fuseri.model.dto.common.DomainObjectDto;

import java.time.Instant;

/**
 * This class represents a Data Transfer Object (DTO) for Certificate entities.
 * It is used to transfer Certificate data between different layers of the application.
 * It extends the DomainObjectDto class and includes additional Certificate-specific fields.
 */
@Getter
@Setter
public class CertificateGenerateDto extends DomainObjectDto {
    @NotBlank
    @NotNull
    private Long userId;
    @NotBlank
    @NotNull
    private Instant generatedAt;
    @NotBlank
    @NotNull
    private Long courseId;
    @NotBlank
    @NotNull
    private String certificateFileName;

    public CertificateGenerateDto(Long id, Long userId, Instant generatedAt, Long courseId, String certificateFileName) {
        this.setId(id);
        this.userId = userId;
        this.generatedAt = generatedAt;
        this.courseId = courseId;
        this.certificateFileName = certificateFileName;
    }
}

