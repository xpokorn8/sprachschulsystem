package org.fuseri.model.dto.mail;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MailDto {

    @NotBlank
    public
    String receiver;
    @NotBlank
    public
    String content;
}
