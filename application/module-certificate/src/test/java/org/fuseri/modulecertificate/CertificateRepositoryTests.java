package org.fuseri.modulecertificate;

import org.fuseri.modulecertificate.certificate.Certificate;
import org.fuseri.modulecertificate.certificate.CertificateRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;
import java.util.List;

@DataJpaTest
class CertificateRepositoryTests {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private CertificateRepository certificateRepository;

    @Test
    void saveCertificate() {
        Certificate certificate = new Certificate();
        certificate.setUserId(1L);
        certificate.setCourseId(2L);

        Certificate saved = certificateRepository.save(certificate);

        Assertions.assertNotNull(saved);
        Assertions.assertEquals(certificate, saved);
    }

    @Test
    void findCertificate() {
        Certificate certificate = new Certificate();
        certificate.setUserId(1L);
        entityManager.persist(certificate);
        entityManager.flush();

        Certificate found = certificateRepository.findById(certificate.getId()).orElse(null);

        Assertions.assertNotNull(found);
        Assertions.assertEquals(found, certificate);
    }

    @Test
    void findCertificateByUserId() {
        Certificate certificate = new Certificate();
        certificate.setUserId(1L);
        entityManager.persist(certificate);
        entityManager.flush();

        List<Certificate> found = certificateRepository.findCertificateByUserId(1L);

        Assertions.assertEquals(1, found.size());
        Assertions.assertEquals(found.get(0), certificate);
    }

    @Test
    void findCertificateByUserIdAndCourseId() {
        Certificate certificate = new Certificate();
        certificate.setUserId(1L);
        certificate.setCourseId(2L);
        entityManager.persist(certificate);
        entityManager.flush();

        List<Certificate> found = certificateRepository.findCertificateByUserIdAndCourseId(1L, 2L);

        Assertions.assertEquals(1, found.size());
        Assertions.assertEquals(found.get(0), certificate);
    }

    @Test
    public void testFindAllCertificates() {
        Certificate certificate1 = new Certificate();
        Certificate certificate2 = new Certificate();

        certificateRepository.save(certificate1);
        certificateRepository.save(certificate2);

        Page<Certificate> certificatePage = certificateRepository.findAll(PageRequest.of(0, 42));

        Assertions.assertEquals(2, certificatePage.getTotalElements());
        Assertions.assertEquals(certificatePage.getContent(), Arrays.asList(certificate1, certificate2));
    }

    @Test
    public void testDeleteCertificate() {
        Long certificateId = entityManager.persist(new Certificate()).getId();
        entityManager.flush();

        certificateRepository.deleteById(certificateId);

        Assertions.assertTrue(certificateRepository.findById(certificateId).isEmpty());
    }


}
