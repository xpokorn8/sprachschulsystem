package org.fuseri.modulecertificate.datainitializer;

import org.fuseri.modulecertificate.certificate.Certificate;
import org.fuseri.modulecertificate.certificate.CertificateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component
public class DataInitializer {

    private final CertificateRepository certificateRepository;

    @Autowired
    public DataInitializer(CertificateRepository certificateRepository) {
        this.certificateRepository = certificateRepository;
    }

    public void drop() {
        certificateRepository.deleteAll();
    }

    public void initialize() {
        drop();

        Certificate c1 = new Certificate(7L, Instant.parse("2023-04-12T10:30:00Z"), 2L, "certificate_file_1.pdf");
        Certificate c2 = new Certificate(7L, Instant.parse("2023-04-17T14:15:00Z"), 3L, "certificate_file_3.pdf");
        Certificate c3 = new Certificate(9L, Instant.parse("2023-04-23T14:15:00Z"), 3L, "certificate_file_4.pdf");
        Certificate c4 = new Certificate(7L, Instant.parse("2023-04-28T14:15:00Z"), 4L, "certificate_file_5.pdf");
        Certificate c5 = new Certificate(9L, Instant.parse("2023-04-29T14:15:00Z"), 4L, "certificate_file_6.pdf");

        certificateRepository.saveAll(List.of(c1, c2, c3, c4, c5));
    }
}
