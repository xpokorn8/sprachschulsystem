package org.fuseri.modulecertificate.certificate;

import org.fuseri.model.dto.certificate.CertificateCreateDto;
import org.fuseri.model.dto.certificate.CertificateGenerateDto;
import org.fuseri.model.dto.certificate.CertificateSimpleDto;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.time.Instant;
import java.util.List;

@Mapper(componentModel = "spring")
public interface CertificateMapper {

    default CertificateSimpleDto mapToSimpleDto(Certificate certificate)
    {
        if ( certificate == null ) {
            return null;
        }

        return new CertificateSimpleDto(
                certificate.getId(),
                certificate.getUserId(),
                certificate.getGeneratedAt(),
                certificate.getCourseId(),
                certificate.getCertificateFileName());
    }

    default CertificateGenerateDto mapToCertificateGenerateDto(Certificate certificate)
    {
        if ( certificate == null ) {
            return null;
        }

        return new CertificateGenerateDto(
                certificate.getId(),
                certificate.getUserId(),
                certificate.getGeneratedAt(),
                certificate.getCourseId(),
                certificate.getCertificateFileName());
    }


    default Certificate mapToCertificate(CertificateCreateDto certificateCreateDto,Instant generatedAt, String fileName)
    {
        if ( certificateCreateDto == null ) {
            return null;
        }


        return new Certificate(certificateCreateDto.getUser().getId(),
                generatedAt,
                certificateCreateDto.getCourse().getId(),
                fileName);
    }

    List<CertificateSimpleDto> mapToList(List<Certificate> certificates);

    default Page<CertificateSimpleDto> mapToPageDto(Page<Certificate> certificates) {
        return new PageImpl<>(mapToList(certificates.getContent()), certificates.getPageable(), certificates.getTotalPages());
    }
}
