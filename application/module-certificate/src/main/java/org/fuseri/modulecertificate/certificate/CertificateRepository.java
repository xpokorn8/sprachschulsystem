package org.fuseri.modulecertificate.certificate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface CertificateRepository extends JpaRepository<Certificate, Long> {

    @Query("SELECT c FROM Certificate c WHERE c.userId = ?1")
    List<Certificate> findCertificateByUserId(Long userId);

    @Query("SELECT c FROM Certificate c WHERE c.userId = ?1 and c.courseId = ?2")
    List<Certificate> findCertificateByUserIdAndCourseId(Long userId, Long courseId);

}

