package org.fuseri.modulecertificate.certificate;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

@Entity
@Table(name = "certificate")
public class Certificate implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_certificate")
    private Long id;
    @Column(name = "id_user")
    private Long userId;
    @Column(name = "generatedAt")
    @Temporal(TemporalType.TIMESTAMP)
    private Instant generatedAt;
    @Column(name = "id_course")
    private Long courseId;
    @Column(name = "certificate_file_name")
    private String certificateFileName;

    public Certificate() {
    }

    public Certificate(Long userId, Instant generatedAt, Long courseId, String certificateFileName) {
        this.userId = userId;
        this.generatedAt = generatedAt;
        this.courseId = courseId;
        this.certificateFileName = certificateFileName;
    }

    public Long getId() {
        return id;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Instant getGeneratedAt() {
        return generatedAt;
    }

    public void setGeneratedAt(Instant generatedAt) {
        this.generatedAt = generatedAt;
    }

    public Long getCourseId() {
        return courseId;
    }

    public void setCourseId(Long courseId) {
        this.courseId = courseId;
    }


    public String getCertificateFileName() {
        return certificateFileName;
    }

    public void setCertificateFileName(String certificateFileName) {
        this.certificateFileName = certificateFileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Certificate certificate)) {
            return false;
        }
        return Objects.equals(getUserId(), certificate.getUserId())
                && Objects.equals(getGeneratedAt(), certificate.getGeneratedAt())
                && Objects.equals(getCourseId(), certificate.getCourseId())
                && Objects.equals(getCertificateFileName(), certificate.getCertificateFileName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getGeneratedAt(), getCourseId(), getCertificateFileName());
    }
}

