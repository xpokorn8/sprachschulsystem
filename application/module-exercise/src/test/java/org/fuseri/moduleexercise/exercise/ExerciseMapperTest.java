package org.fuseri.moduleexercise.exercise;

import org.fuseri.model.dto.exercise.ExerciseCreateDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ExerciseMapperTest {

    private final ExerciseCreateDto exerciseCreateDto = new ExerciseCreateDto(
            "History exercise", "Great", 2, 4);


    @Autowired
    private ExerciseMapper exerciseMapper;

    @Test
    void mapNullEntityToDto() {
        var entity = exerciseMapper.fromCreateDto(null);
        Assertions.assertNull(entity);
    }

    @Test
    void mapFromNullCreateDto() {
        var entity = exerciseMapper.fromCreateDto(null);
        Assertions.assertNull(entity);
    }

    @Test
    void mapFromCreateDto() {
        var entity = exerciseMapper.fromCreateDto(exerciseCreateDto);
        Assertions.assertNotNull(entity);
    }




    @Test
    void mapNullToDto() {
        var createdDto = exerciseMapper.toDto(null);
        Assertions.assertNull(createdDto);
    }


}
