package org.fuseri.moduleexercise.answer;

import org.fuseri.model.dto.exercise.AnswerCreateDto;
import org.fuseri.model.dto.exercise.AnswerDto;
import org.fuseri.moduleexercise.exercise.Exercise;
import org.fuseri.moduleexercise.question.Question;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
class AnswerFacadeTest {

    @Autowired
    private AnswerFacade answerFacade;

    @MockBean
    private AnswerService answerService;

    @MockBean
    private AnswerMapper answerMapper;

    private final AnswerDto answerDto = new AnswerDto("name", true);
    private final AnswerCreateDto answerCreateDto = new AnswerCreateDto("name", true, 1L);
    private final Question question = new Question("text", new HashSet<>(), new Exercise());
    private final Answer answer = new Answer("name", true, question);

    @Test
    void update() {
        long id = 1L;
        when(answerMapper.fromCreateDto(answerCreateDto)).thenReturn(answer);
        when(answerService.update(id, answer)).thenReturn(answer);
        when(answerMapper.toDto(answer)).thenReturn(answerDto);

        AnswerDto actualDto = answerFacade.update(id, answerCreateDto);
        assertEquals(answerDto, actualDto);
    }

    @Test
    void delete() {
        long id = 1L;
        answerFacade.delete(id);
        verify(answerService).delete(id);
    }

}
