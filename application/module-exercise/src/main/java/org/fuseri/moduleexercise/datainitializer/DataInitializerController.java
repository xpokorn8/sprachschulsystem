package org.fuseri.moduleexercise.datainitializer;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/datainitializer")
public class DataInitializerController {
    private final DataInitializer dataInitializer;

    @Autowired
    public DataInitializerController(DataInitializer dataInitializer) {
        this.dataInitializer = dataInitializer;
    }

    @Operation(summary = "Seed exercise database", description = "Seeds exercise database. Drops all data first")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Data initialized successfully."),
    })
    @PostMapping
    public ResponseEntity<Void> initialize() {
        dataInitializer.initialize();
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Drop exercise database", description = "Drops all data from exercise database")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Data dropped successfully."),
    })
    @DeleteMapping
    public ResponseEntity<Void> drop() {
        dataInitializer.drop();
        return ResponseEntity.noContent().build();
    }

}
