package org.fuseri.moduleexercise.answer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * A repository interface for managing Answer entities
 */
@Repository
public interface AnswerRepository extends JpaRepository<Answer, Long> {

    /**
     * Find all answers to a question with the specified ID
     *
     * @param questionId the ID of the question to find answers for
     * @return a list of all answers to the specified question
     */
    List<Answer> findByQuestionId(@Param("questionId") long questionId);
}