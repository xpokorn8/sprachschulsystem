package org.fuseri.moduleexercise.answer;

import jakarta.persistence.*;
import lombok.*;
import org.fuseri.moduleexercise.common.DomainObject;
import org.fuseri.moduleexercise.question.Question;

/**
 * Represent Answer entity
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "answer")
public class Answer extends DomainObject {

    private String text;

    @Column(name = "is_correct")
    private boolean correct;

    @ManyToOne
    @JoinColumn(name="question_id")
    private Question question;
}
