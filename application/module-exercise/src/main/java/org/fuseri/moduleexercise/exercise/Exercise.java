package org.fuseri.moduleexercise.exercise;

import jakarta.persistence.*;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.fuseri.moduleexercise.common.DomainObject;
import org.fuseri.moduleexercise.question.Question;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Represent exercise entity
 */
@Getter
@Setter
@NoArgsConstructor
@Builder
@Entity
@Table(name = "exercise")
public class Exercise extends DomainObject {

    private String name;

    private String description;

    private int difficulty;

    @Column(name = "course_id")
    private long courseId;

    @OneToMany(mappedBy="exercise", cascade = CascadeType.ALL)
    private Set<Question> questions = new HashSet<>();

    /**
     * Constructor of exercise
     *
     * @param name        exercise name
     * @param description exercise description
     * @param difficulty  exercise's difficulty
     * @param courseId    id of lecture to which exercise belongs
     * @param questions   question exercise contains
     */
    public Exercise(String name, String description, int difficulty, long courseId, Set<Question> questions) {
        this.name = name;
        this.description = description;
        this.difficulty = difficulty;
        this.courseId = courseId;
        this.questions = Objects.requireNonNullElseGet(questions, HashSet::new);
    }
}
