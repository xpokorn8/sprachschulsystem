package org.fuseri.moduleexercise.question;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * A repository interface for managing Question entities
 */
@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {
}