package org.fuseri.moduleexercise.common;

import org.fuseri.model.dto.common.DomainObjectDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

/**
 * Base interface for MapStruct Mappers
 * It defines methods to convert DTOs to entities and vice versa
 *
 * @param <T> the type of the entity
 * @param <S> the type of the DTO
 */
public interface DomainMapper<T extends DomainObject, S extends DomainObjectDto> {

    /**
     * Convert DTO to its corresponding entity
     *
     * @param dto DTO to be converted
     * @return corresponding entity created from DTO
     */
    T fromDto(S dto);

    /**
     * Convert entity to its corresponding DTO
     *
     * @param entity entity to be converted
     * @return corresponding DTO created from entity
     */
    S toDto(T entity);

    /**
     * Convert List of entities to List of corresponding DTOs
     *
     * @param entities entities to be converted
     * @return corresponding list of DTO objects
     */
    List<S> toDtoList(List<T> entities);

    /**
     * Convert a {@link org.springframework.data.domain.Page} containing entities to a page containing DTOs
     *
     * @param entities entities to be converted to DTOs
     * @return a Page object containing a list of DTO objects and pagination information
     */
    default Page<S> toDtoPage(Page<T> entities) {
        return new PageImpl<>(toDtoList(entities.getContent()), entities.getPageable(), entities.getTotalPages());
    }

}
