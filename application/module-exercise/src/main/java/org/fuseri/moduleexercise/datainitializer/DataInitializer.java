package org.fuseri.moduleexercise.datainitializer;

import org.fuseri.moduleexercise.answer.Answer;
import org.fuseri.moduleexercise.answer.AnswerRepository;
import org.fuseri.moduleexercise.exercise.Exercise;
import org.fuseri.moduleexercise.exercise.ExerciseRepository;
import org.fuseri.moduleexercise.question.Question;
import org.fuseri.moduleexercise.question.QuestionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;

@Component
public class DataInitializer {

    private final ExerciseRepository exerciseRepository;
    private final QuestionRepository questionRepository;
    private final AnswerRepository answerRepository;

    @Autowired
    public DataInitializer(ExerciseRepository exerciseRepository, QuestionRepository questionRepository,
                           AnswerRepository answerRepository) {
        this.exerciseRepository = exerciseRepository;
        this.questionRepository = questionRepository;
        this.answerRepository = answerRepository;
    }

    public void drop() {
        exerciseRepository.deleteAll();
    }

    public void initialize() {
        drop();

        // exercise
        var ex1 = new Exercise("Present Continuous Exercise 1", "Practice using the present continuous tense.", 6, 5, new HashSet<>());
        var ex2 = new Exercise("Past Simple Exercise 1", "Use the past simple to describe past events.", 6, 5, new HashSet<>());
        var ex3 = new Exercise("Food Vocabulary Exercise 1", "Learn vocabulary related to food.", 2, 5, new HashSet<>());
        var ex4 = new Exercise("Job Interview Exercise 1", "Practice answering common job interview questions.", 7, 5, new HashSet<>());
        var ex5 = new Exercise("Begrüßung und Vorstellung Übung", "Übung zum Begrüßen und Vorstellen", 2, 1, new HashSet<>());
        exerciseRepository.saveAll(List.of(ex1, ex2, ex3, ex4, ex5));

        // question
        var q1 = new Question("What is the correct form of the present continuous tense of \"run\"?", new HashSet<>(), ex1);
        var q2 = new Question("Which verbs are not usually used in the present continuous tense?", new HashSet<>(), ex1);
        var q3 = new Question("What did you do last weekend?", new HashSet<>(), ex2);
        var q4 = new Question("What did you eat for breakfast this morning?", new HashSet<>(), ex2);
        var q5 = new Question("Where did you go on vacation last year?", new HashSet<>(), ex2);
        var q6 = new Question("What is the English word for \"la comida\"?", new HashSet<>(), ex3);
        var q7 = new Question("Tell me about yourself.", new HashSet<>(), ex4);
        var q8 = new Question("Why are you interested in this job?", new HashSet<>(), ex4);
        var q9 = new Question("What are your greatest strengths and weaknesses?", new HashSet<>(), ex4);
        var q10 = new Question("Can you give an example of a time when you overcame a challenge?", new HashSet<>(), ex4);
        var q11 = new Question("Where do you see yourself in five years?", new HashSet<>(), ex4);
        var q12 = new Question("Was ist eine höfliche Begrüßung im Deutschen?", new HashSet<>(), ex5);
        var q13 = new Question("Wie antwortet man auf die Frage \"Wie geht es Ihnen?\"", new HashSet<>(), ex5);
        var q14 = new Question("Wie stellt man sich auf Deutsch vor?", new HashSet<>(), ex5);

        var questions = List.of(q1, q2, q3, q4, q5, q6, q7, q8, q9, q10, q11, q12, q13, q14);
        questionRepository.saveAll(questions);

        // answer
        var a1 = new Answer("is running", true, q1);
        var a2 = new Answer("are running", false, q1);
        var a3 = new Answer("am running", false, q1);

        var a4 = new Answer("think", true, q2);
        var a5 = new Answer("want", true, q2);
        var a6 = new Answer("love", false, q2);

        var a7 = new Answer("I visited my family.", true, q3);
        var a8 = new Answer("I'm visiting my family.", false, q3);
        var a9 = new Answer("I will visit my family.", false, q3);

        var a10 = new Answer("the food", true, q4);
        var a11 = new Answer("the drink", false, q4);
        var a12 = new Answer("the music", false, q4);

        var a13 = new Answer("I ate cereal for breakfast.", true, q5);
        var a14 = new Answer("I eat cereal for breakfast.", false, q5);
        var a15 = new Answer("I will eat cereal for breakfast.", false, q5);

        var a16 = new Answer("I went to Hawaii on vacation last year.", true, q6);
        var a17 = new Answer("I'm going to Hawaii on vacation next year.", false, q6);
        var a18 = new Answer("I will go to Hawaii on vacation next year.", false, q6);

        var a19 = new Answer("I was born in a small town in the midwest.", false, q7);
        var a20 = new Answer("I graduated from XYZ University with a degree in computer science.", false, q7);
        var a21 = new Answer("I have over five years of experience in software development.", true, q7);

        var a22 = new Answer("I just need a job.", false, q8);
        var a23 = new Answer("I am excited about the company culture and the opportunities for growth.", true, q8);
        var a24 = new Answer("I heard the salary is good.", false, q8);

        var a25 = new Answer("My greatest strength is my attention to detail. My greatest weakness is public speaking.", true, q9);
        var a26 = new Answer("My greatest strength is my ability to multitask. My greatest weakness is that I am a perfectionist.", false, q9);
        var a27 = new Answer("My greatest strength is my leadership skills. My greatest weakness is that I work too hard.", false, q9);

        var a28 = new Answer("When I was working on a project for a client, the client requested changes that were outside the scope of the project. I had to work with the client to manage their expectations and negotiate a compromise.", true, q10);
        var a29 = new Answer("When I was in college, I had a difficult time balancing my coursework and my social life. I had to learn how to manage my time better.", false, q10);
        var a30 = new Answer("I have never faced a significant challenge in my professional life.", false, q10);

        var a31 = new Answer("I see myself in a management position, leading a team of developers.", false, q11);
        var a32 = new Answer("I see myself as a valuable contributor to the company, continuing to develop my skills and taking on new challenges.", true, q11);

        var a33 = new Answer("Guten Tag!", true, q12);
        var a34 = new Answer("Hallo!", false, q12);
        var a35 = new Answer("Moin!", false, q12);

        var a36 = new Answer("Mir geht es gut, danke!", true, q13);
        var a37 = new Answer("Mir geht es schlecht, danke!", false, q13);
        var a38 = new Answer("Mir geht es so lala.", false, q13);

        var a39 = new Answer("Ich heiße [Name].", true, q14);
        var a40 = new Answer("Ich bin [Name].", false, q14);
        var a41 = new Answer("Ich komme aus [Land].", false, q14);

        var answers = List.of(
                a1, a2, a3, a4, a5, a6, a7, a8, a9, a10, a11, a12, a13, a14, a15,
                a16, a17, a18, a19, a20, a21, a22, a23, a24, a25, a26, a27, a28,
                a29, a30, a31, a32, a33, a34, a35, a36, a37, a38, a39, a40, a41);

        answerRepository.saveAll(answers);
    }
}
