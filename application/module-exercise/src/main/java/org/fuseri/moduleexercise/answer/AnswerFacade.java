package org.fuseri.moduleexercise.answer;

import jakarta.transaction.Transactional;
import org.fuseri.model.dto.exercise.AnswerCreateDto;
import org.fuseri.model.dto.exercise.AnswerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Represent facade for managing answers
 * Provide simplified interface for manipulating with answers
 */
@Service
@Transactional
public class AnswerFacade {
    private final AnswerService answerService;
    private final AnswerMapper mapper;

    /**
     * Constructor for AnswerFacade
     *
     * @param answerService the service responsible for handling answer-related logic
     * @param mapper        the mapper responsible for converting between DTOs and entities
     */
    @Autowired
    public AnswerFacade(AnswerService answerService, AnswerMapper mapper) {
        this.answerService = answerService;
        this.mapper = mapper;
    }

    /**
     * Create a new answer for the given question ID
     *
     * @param dto the AnswerCreateDto object containing information about the answer to create
     * @return an AnswerDto object representing the newly created answer
     */
    public AnswerDto create(@RequestBody AnswerCreateDto dto) {
        return mapper.toDto(answerService.create(mapper.fromCreateDto(dto)));
    }

    /**
     * Update answer
     *
     * @param id  of answer to update
     * @param dto dto with updated answer information
     */
    public AnswerDto update(long id, AnswerCreateDto dto) {
        return mapper.toDto(answerService.update(id, mapper.fromCreateDto(dto)));
    }

    /**
     * Delete answer with the given id
     *
     * @param id of answer to delete
     */
    public void delete(long id) {
        answerService.delete(id);
    }
}
