package org.fuseri.moduleexercise.exercise;

import org.fuseri.moduleexercise.question.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * A repository interface for managing Exercise entities
 */
@Repository
public interface ExerciseRepository extends JpaRepository<Exercise, Long> {

    /**
     * Filters the exercises by the specified difficulty level and course id,
     * and returns an object containing these filtered exercises
     * along with pagination information
     *
     * @param courseId   the id of the course to filter by
     * @param difficulty the difficulty level to filter by
     * @param pageable   the pagination settings
     * @return object containing a list of paginated exercises that match the filter criteria
     */
    @Query("SELECT e FROM Exercise e WHERE e.courseId = :courseId AND e.difficulty = :difficulty")
    Page<Exercise> findByCourseIdAndDifficulty(long courseId, int difficulty, Pageable pageable);

    @Query("SELECT q FROM Exercise e JOIN e.questions q WHERE e.id = :exerciseId")
    Page<Question> getQuestions(PageRequest pageRequest, @Param("exerciseId") Long exerciseId);

}
