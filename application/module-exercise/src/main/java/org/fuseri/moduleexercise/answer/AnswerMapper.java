package org.fuseri.moduleexercise.answer;

import org.fuseri.model.dto.exercise.AnswerCreateDto;
import org.fuseri.model.dto.exercise.AnswerDto;
import org.fuseri.model.dto.exercise.AnswerInQuestionCreateDto;
import org.fuseri.moduleexercise.common.DomainMapper;
import org.fuseri.moduleexercise.question.Question;
import org.fuseri.moduleexercise.question.QuestionService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

/**
 * Mapper between Answers and their corresponding DTOs
 */
@Mapper(componentModel = "spring", uses = {QuestionService.class})
public abstract class AnswerMapper implements DomainMapper<Answer, AnswerDto> {

    @Autowired
    private QuestionService questionService;

    @Named("mapIdToQuestion")
    public Question mapIdToQuestion(Long id) {
        return questionService.find(id);
    }

    /**
     * Convert DTO of type AnswerCreateDto to Answer
     *
     * @param dto DTO to be converted
     * @return corresponding Answer entity created from DTO
     */
    @Mapping(target = "question", source = "dto.questionId", qualifiedByName = "mapIdToQuestion")
    public abstract Answer fromCreateDto(AnswerCreateDto dto);

    /**
     * Convert Set of AnswerInQuestionCreateDto to List of corresponding Answers
     *
     * @param dtos to be converted
     * @return corresponding list of Answers
     */
    public abstract Set<Answer> fromCreateDtoList(List<AnswerInQuestionCreateDto> dtos);

    /**
     * Convert DTO of type AnswerInQuestionCreateDto to Answer
     *
     * @param dto DTO to be converted
     * @return corresponding Answer entity created from DTO
     */
    public abstract Answer fromCreateDto(AnswerInQuestionCreateDto dto);
}
