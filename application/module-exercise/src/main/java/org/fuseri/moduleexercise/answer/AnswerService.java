package org.fuseri.moduleexercise.answer;

import jakarta.persistence.EntityNotFoundException;
import lombok.Getter;
import org.fuseri.moduleexercise.common.DomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * Represent a service for managing Answer entities
 */
@Service
public class AnswerService extends DomainService<Answer> {

    /**
     * The repository instance used by this service
     */
    @Getter
    private final AnswerRepository repository;

    /**
     * Construct a new instance of AnswerService with the specified repository
     *
     * @param repository the repository instance to be used by this service
     */
    @Autowired
    public AnswerService(AnswerRepository repository) {
        this.repository = repository;
    }

    /**
     * Retrieve the Answer entity with the specified id
     *
     * @param id the id of the Answer entity to retrieve
     * @return the Answer entity with the specified id
     * @throws EntityNotFoundException if no Answer entity exists with the specified id
     */
    @Transactional(readOnly = true)
    public Answer find(long id) {
        return repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Answer '" + id + "' not found."));
    }
}
