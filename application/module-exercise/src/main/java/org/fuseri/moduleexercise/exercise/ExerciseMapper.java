package org.fuseri.moduleexercise.exercise;

import org.fuseri.model.dto.course.CourseDto;
import org.fuseri.model.dto.exercise.ExerciseCreateDto;
import org.fuseri.model.dto.exercise.ExerciseDto;
import org.fuseri.moduleexercise.common.DomainMapper;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.List;

/**
 * Mapper between Exercises and their corresponding DTOs
 */
@Mapper
public interface ExerciseMapper extends DomainMapper<Exercise, ExerciseDto> {

    /**
     * Convert DTO of type ExerciseCreateDto to Exercise
     *
     * @param dto DTO to be converted
     * @return corresponding Exercise entity created from DTO
     */
    Exercise fromCreateDto(ExerciseCreateDto dto);

    List<ExerciseDto> mapToList(List<Exercise> persons);

    default Page<ExerciseDto> mapToPageDto(Page<Exercise> courses) {
        return new PageImpl<>(mapToList(courses.getContent()), courses.getPageable(), courses.getTotalPages());
    }
}
