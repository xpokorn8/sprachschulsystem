package org.fuseri.modulemail.service;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.validation.Valid;
import org.fuseri.model.dto.mail.MailDto;
import org.fuseri.modulemail.ModuleMailApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/mail")
public class MailController {

    private final MailService service;

    @Autowired
    public MailController(MailService service) {
        this.service = service;
    }

    @Operation(security = @SecurityRequirement(name = ModuleMailApplication.SECURITY_SCHEME_NAME),
            summary = "Send email", description = "Sends email message with specified texto to specified receiver.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully sent email."),
            @ApiResponse(responseCode = "500", description = "Internal server error."),
    })
    @PostMapping()
    public ResponseEntity<String> sendMail(@Valid @RequestBody MailDto emailDto) {
        return ResponseEntity.ok(service.send(emailDto));
    }
}
