package org.fuseri.modulemail.service;

import org.fuseri.model.dto.mail.MailDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
@Service
public class MailService {

    private final JavaMailSender emailSender;

    @Autowired
    public MailService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    public String send(MailDto dto) {
        var message = new SimpleMailMessage();
        message.setFrom("sprachul@gmail.com");
        message.setTo(dto.receiver);
        message.setSubject("Sprachschul");
        message.setText(dto.content);

        emailSender.send(message);
        // store to database once there is one

        return "Success, you have sent: " + dto.content;
    }

}
