# Sprachschulsystem

*PA165 course project at FI MUNI Spring 2023.*

# About Project

- **Name**: Language school
- **Technologies**: Java 17, Spring, Maven, ...
- **Developers**:
    - Dominika Zemanovičová *@xzemanov*
    - Martin Gargalovič *@xgargal*
    - Jan Pokorný *@xpokorn8*
    - Ester Vilímková - _Project Leader_ *@xvilimk*


- **Assigment**:
    - Create a system for language school lecture management. Each lecture can occur on a given day and time and is related to some course. The course is defined by its (unique) name, language and proficiency level. However, each lecture will be independent. That means that each lecture can have a different topic. Different lecturers can give it, and an arbitrary number of students can enrol. Each lecturer will have a name, surname, and record of taught languages. In addition, the lecturer will indicate that they are a native speaker. Exercises can be prepared for each course to allow the students to learn the language. Each student can pick the exercises depending on the levels of difficulty.


- **Running the modules with metrics and monitoring using docker compose**:

    ```console
    cd ./application
    docker compose up --build
    ```

- **Running just the modules using docker**:
    ~~~console
   sudo docker run -d -p 8080:8080 xpokorn8/sprachschulsystem:confidential-client &&
   sudo docker run -d -p 8082:8082 xpokorn8/sprachschulsystem:certificate &&
   sudo docker run -d -p 8083:8083 xpokorn8/sprachschulsystem:exercise &&
   sudo docker run -d -p 8081:8081 xpokorn8/sprachschulsystem:language-school &&
   sudo docker run -d -p 8084:8084 xpokorn8/sprachschulsystem:mail
    ~~~
  
- **Security**:
  - Run ```ConfidentialClient``` module. Call ```/register``` [endpoint](http://localhost:8080/register) to register in the LanguageSchool with MUNI unified login. Make sure that module ```LanguageSchool``` is also running, otherwise you will get connection error.
    - Select the scopes you want: 
      - ```test_1``` = admin,
      - ```test_2``` = lecturer,
      - no scopes = student.
    - You will be registered in the language school upon successful sign-on via MUNI login.
  - Call ```/token``` [endpoint](http://localhost:8080/token) to retrieve the bearer token.
    - Copy the displayed bearer token and paste it into the Swagger token authentication option (green lock button in the top right corner) or use the token in auth options of Postman.

# Metrics and Monitoring Dashboards
Our application uses Prometheus and Grafana to collect metrics and display them.
When you use the above specified docker-compose commands to run the app, both Prometheus and Grafana containers are automatically launched alongside the app's modules.

You can find the **Prometheus** interface on
http://localhost:9090

You can find **Grafana** interface on http://localhost:3000<br />
The credentials are:<br />
**username:** admin <br />
**password:** admin <br />

In Grafana interface, you can import various dashboards. Our team used and tested https://grafana.com/grafana/dashboards/3662-prometheus-2-0-overview/ board.<br />
To import the dashboard, follow steps bellow:

![Import 1](https://gitlab.fi.muni.cz/xpokorn8/sprachschulsystem/-/raw/main/docs/grafana01.png)
Select **+** and **Import** in left menu of the Grafana interface.

![Import 2](https://gitlab.fi.muni.cz/xpokorn8/sprachschulsystem/-/raw/main/docs/grafana02.png)
Insert **3662** into **Import via grafana.com** and click **Load**.

![Prometheus Datasource](https://gitlab.fi.muni.cz/xpokorn8/sprachschulsystem/-/raw/main/docs/grafana03.png)
Select **Prometheus** in the **prometheus** combo box and click **Import**.

![Dashboard Displayed](https://gitlab.fi.muni.cz/xpokorn8/sprachschulsystem/-/raw/main/docs/grafana04.png)
The dashboard with metrics data will be displayed.

# Project Description

Sprachschulsystem is a web-based application that aims to simplify and streamline the management of language school lectures. The system is designed to provide an easy-to-use interface that allows language schools to manage lectures, courses, lecturers, exercises, and students.

## Scopes

System uses three scopes:
- **Student** - default scope,
- **Admin** -  ```test_1```,
- **Lecturer** - ```test_2```.

## Module services

- **confidentialClient** (port 8080)
- **module-language-school** (port 8081)
- **module-certificate** (port 8082)
- **module-exercise** (port 8083)
- **module-mail** (port 8084)

# System Scenarios

## Enrollment and Lecture Selection Process
A lecturer authenticates into the system, creates a few courses with arbitrary names, capacities, languages and proficiencies, and adds themself to those courses. The lecturer then creates corresponding lectures for those courses, assigning them to specific date-times and appointing them a topic and capacity.

Meanwhile, an arbitrary number of students authenticate into the system and access a list of available courses for enrollment. They browse through the list of courses, read the descriptions, and select the ones they are interested in. Once they choose, they try to enrol in those courses. The system checks if any seats are left, and if there are, the students are successfully enrolled.

After enrolling in the courses, the students browse through the list of lectures for their enrolled courses and select the ones they would like to attend. They check the dates and times of the lectures, and if they find a suitable one, they enrol in that lecture. Once again, the system checks if there are any available seats for the selected lectures and if there are, the students are successfully enrolled.

_**tldr;**_ scenario mimics the _cliché_ school system lecture/course enrollment process, where students _fight_ against others for any available seats in courses and lectures they are interested in. It also demonstrates the ability of the system to handle multiple users simultaneously, ensuring that the enrollment process is seamless and hassle-free.

- Prerequisite is having locust installed - please refer to documentation https://docs.locust.io/en/stable/installation.html
- Starting locust (while running all system modules):
    ~~~console
    cd ./application
    locust
    ~~~

- Navigate to http://localhost:8089
- Enter following data for basic showcasing the app usage
  - Number of users: 3
  - Spawn rate: 1
  - Host: http://localhost:8081
  - Token: enter token retrieved from http://localhost:8080/token (see corresponding section of this readme)
- Enter following data for simulating high system load
  - Number of users: 60 (or try more)
  - Spawn rate: 5
  - Host: http://localhost:8081
  - Token: enter token retrieved from http://localhost:8080/token (see corresponding section of this readme)
- Switch tabs to explore the current state of the API

# Diagrams

DTO Class Diagram:
![Class Diagram](https://gitlab.fi.muni.cz/xpokorn8/sprachschulsystem/-/raw/main/docs/ClassDiagram.png)

---

Use-case Diagram:
![UseCase Diagram](https://gitlab.fi.muni.cz/xpokorn8/sprachschulsystem/-/raw/main/docs/UseCaseDiagram.png)
